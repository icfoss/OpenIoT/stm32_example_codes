# Prerequisite
- STM32 Cube IDE [here](https://www.st.com/en/development-tools/stm32cubeide.html)

# Getting Started
- Create a workspace inside STM32 Cube IDE
- Import the project to the workspace
- Build project in the toolchain
- Configure the keys in se-identity.h
- Set the Transmission Interval and Activation type (ABP/OTA) in lora_app.h
- Write the corresponding source and header file for the sensors
- Include the sensor header in lora_app.c and sys_app.c
- Inside sys_app.c call the initialisation function for the sensors inside SystemApp_Init()
- Inside lora_app.c initialize the structure to hold sensor data, call the function to read sensor data and assign the data in the application buffer for transmission  in SendTxData()
