#include <string.h>
#include <stdlib.h>
#include "hw.h"
#include "util_console.h"
#include "lora.h"
#include "i2c.h"
#include "bmp180_for_stm32_hal.h"



typedef struct{
	float temp;
	float temperature;
	float pressure;
}read_data;

uint16_t readBatteryLevel(void);
//void BMP180_Init(I2C_HandleTypeDef *hi2c);
void BMP_Init(void);
//void BB180();

//void BMP180_SetOversampling(BMP180_OSS oss);
//void BMP180_UpdateCalibrationData(void);
//void MX_I2C1_Init(void);

void BMP180_sensor_read(read_data *tp_data);
