#include "i2c.h"
#include "util_console.h"
#include "bmp180_for_stm32_hal.h"
#include <string.h>
#include <stdlib.h>
#include "hw.h"
#include "lora.h"
#include "bmp.h"

#define R1 10
#define R2 10
I2C_HandleTypeDef hi2c1;
uint16_t readBatteryLevel(void) {
	uint16_t analogValue = 0; /*   adc reading for battery is stored in the variable  */
	float batteryVoltage = 0;
	uint16_t batteryLevel = 0; /*    battery voltage   */

	/* Read battery voltage reading */
	analogValue = HW_AdcReadChannel(ADC_CHANNEL_4);

	/* disable battery voltage reading */


	/*battery voltage = ADC value*Vref*2/4096   --12 bit ADC with voltage divider factor of 2 */
	batteryVoltage = (analogValue * 3.3 * ((R1 + R2) / R2)) / 4096;

	/*multiplication factor of 100 to convert to int from float*/
	batteryLevel = (uint16_t) (batteryVoltage * 100);

	return batteryLevel;
}


void BMP_Init(){
	//BMP180_Init(I2C_HandleTypeDef *hi2c);
    //BMP180_Init(&I2C_HandleTypeDef.hi2c);
	_bmp180_ui2c = &hi2c1;
	BMP180_SetOversampling(BMP180_ULTRA);
	BMP180_UpdateCalibrationData();
}

/*void BMP_Init(){
	MX_I2C1_Init();
	//BMP180_Init();
}*/
void BMP180_sensor_read(read_data *tp_data){
	int32_t  temp = BMP180_GetTemperature();
	int32_t temperature = BMP180_GetRawTemperature();
		 int32_t pressure = BMP180_GetPressure();
         tp_data->temp = temp;
		 tp_data->temperature = temperature;
		 tp_data->pressure = pressure;

}
