# STM32_LED_Button
 
Describes how to control an LED with a button on the C1_Node_revb_v1.0. 

# Getting Started
- Make sure that you have a   C1_Node_revb_v1.0.
- Install STM32CubeIDE.
- Select board : B-L072Z-LRWAN1


# Prerequisites
STM32CubeIDE [Tested]


# How to create new project
- Run STM32CubeIDE tool.
- Click New Project or Menu -> File -> New Project.
- From Board Selector section, filter to select and use  board B-L072Z-LRWAN1
![image](https://gitlab.com/sherindavid/stm32_example_codes/-/raw/master/Example_Codes/Images/l6.png?ref_type=heads)
# Pinout Configuration
- Select Pinout -> clear pinouts
- Configure a GPIO pin for the button input and another GPIO pin for the LED output(PA12 and PB12 as input and output respectively). 
![image](https://gitlab.com/sherindavid/stm32_example_codes/-/raw/master/Example_Codes/Images/lb1.png?ref_type=heads)
- Select 'Device configuration tool code generation'
# Edit main.c to toggle the LED
- To control the LED with a button , add the below functions inside while(1)

 if (HAL_GPIO_ReadPin(GPIOA, GPIO_PIN_12))
	 {
	  HAL_GPIO_WritePin(GPIOB, GPIO_PIN_12, GPIO_PIN_SET);
	 }
	 else
	 {
	  HAL_GPIO_WritePin(GPIOB, GPIO_PIN_12, GPIO_PIN_RESET);
	 }
# For Build and Debug the project
- click on Build button and Debug button on the toolbar (or in the menu, select Project > Build Project then Run > Debug).
- Click on Resume icon to continue the execution.
