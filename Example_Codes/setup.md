# STM32_LED_Blink
 Describes how to generate program and use HAL functions to blink a LED on the C1_Node_revb_v1.0.


# Getting Started
- Make sure that you have a   C1_Node_revb_v1.0.
- Install STM32CubeIDE.
- Select board : B-L072Z-LRWAN1


# Prerequisites
STM32CubeIDE [Tested]


# How to create new project
- Run STM32CubeIDE tool.
- Click New Project or Menu -> File -> New Project.
- From Board Selector section, filter to select and use  board B-L072Z-LRWAN1
![image](https://gitlab.com/sherindavid/stm32_example_codes/-/raw/master/Example_Codes/Images/l6.png?ref_type=heads)
# Pinout Configuration
- Select Pinout -> clear pinouts
- check that LD1 and LD4 is enabled to PB5 and PB7 pin as GPIO_Output. 
![image](https://gitlab.com/sherindavid/stm32_example_codes/-/raw/master/Example_Codes/Images/lbn1.png?ref_type=heads)

![image1](https://gitlab.com/sherindavid/stm32_example_codes/-/raw/master/Example_Codes/Images/lbn2.png?ref_type=heads)
- Select 'Device configuration tool code generation'
# Edit main.c to toggle the LED
- For the LED toggling , add the below functions inside while(1)

 HAL_GPIO_WritePin(GPIOB, GPIO_PIN_5|GPIO_PIN_7, GPIO_PIN_SET);
	  HAL_Delay(1000);
	  HAL_GPIO_WritePin(GPIOB, GPIO_PIN_5|GPIO_PIN_7, GPIO_PIN_RESET);
	  HAL_Delay(1000); 
# For Build and Debug the project
- click on Build button and Debug button on the toolbar (or in the menu, select Project > Build Project then Run > Debug).
- Click on Resume icon to continue the execution.

