# STM32_LDR
Describes STM32 ADC (Analog-to-Digital Converter) communication using an LDR (Light-Dependent Resistor) on the C1_Node_revb_v1.0. typically aims to measure the light intensity with an LDR and convert the analog signal to a digital value using the STM32 microcontroller's ADC module. 

# Getting Started
- Make sure that you have a   C1_Node_revb_v1.0.
- Install STM32CubeIDE.
- Select board : B-L072Z-LRWAN1


# Prerequisites
- STM32CubeIDE [Tested]
- LDR Sensor

# Connection 

![imageldr](https://gitlab.com/sherindavid/stm32_example_codes/-/raw/master/Example_Codes/Images/ldr_stm.png?ref_type=heads)
- Connect LDR VCC to +3V Of C1 board 
- Connect LDR GND to GND Of C1 board 
- Connect LDR Analog pin to PA4 Of C1 board 

# LoRa stack setup
- Create a new Workspace
- Open the LoRa stack in it ![download link](https://gitlab.com/icfoss/OpenIoT/c-1_dev_lorawan_end_node_firmware_v2.1)
- Open 'cmwx1zzabz_0xx' file
- To configure LoRaWAN Keys
Select - LoRaWAN_End_Node/LoRaWAN/App/se-identity.h
- To set the Transmission Interval in milliseconds
Select - LoRaWAN_End_Node/LoRaWAN/App/lora_app.h
- Create .h and .c file for the sensor in 'Application' folder
- Include the .h file in lora_app.c
- Include 'adc_if.h' file to read the ADC value from the sensor in lora_app.c.
- 'ADC_ReadChannels()' Reads the ADC value from the sensor
- For transmitting sensor data to the server, use'static void SendTxData(void)'function.

# For Build and Debug the project
- click on Build button and Debug button on the toolbar (or in the menu, select Project > Build Project then Run > Debug).
- Click on Resume icon to continue the execution.
