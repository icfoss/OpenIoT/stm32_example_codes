# STM32_Example_Codes
STM32 example codes serve as valuable resources for developers working with STM32 microcontrollers. These codes are typically snippets or complete programs that showcase specific functionalities, features, or configurations of the STM32 microcontroller family. They are designed to help developers understand how to use various peripherals, implement specific communication protocols, handle interrupts, or perform other tasks efficiently. These examples often come with detailed comments and explanations, aiding in the learning process. STM32 example codes can be found in the STM32Cube software development platform, STM32CubeMX, or other development environments. They provide a hands-on approach for developers to quickly grasp and implement specific functionalities in their STM32-based projects, saving time and ensuring proper utilization of the microcontroller's capabilities. Overall, these example codes contribute significantly to the ease of development and the rapid prototyping of embedded systems using STM32 microcontrollers.

# Getting Started
- Make sure that you have a C1_Node_revb_v1.0.
- Install STM32CubeIDE.
- Select board : B-L072Z-LRWAN1
- How to flash the code 
using [stm programmer module](https://gitlab.com/sherindavid/stm32_example_codes/-/blob/master/Example_Codes/Connection_diagram/Pgm_connection_STMProgrammer.pdf?ref_type=heads)
using [stm32 module](https://gitlab.com/sherindavid/stm32_example_codes/-/blob/master/Example_Codes/Connection_diagram/Pgm_conection_STM32board.pdf?ref_type=heads)
- How to [blink an LED](https://gitlab.com/sherindavid/stm32_example_codes/-/blob/master/Example_Codes/setup.md?ref_type=heads)
- How to [control an LED with Button](https://gitlab.com/sherindavid/stm32_example_codes/-/blob/master/Example_Codes/setup1.md?ref_type=heads)
- How to [Interface LDR with LoRaWAN Stack](https://gitlab.com/sherindavid/stm32_example_codes/-/blob/master/Example_Codes/setup2.md?ref_type=heads)
# Prerequisites
STM32CubeIDE [Tested]

# Contributing
Instructions coming up soon.

# License
This project is licensed under the MIT License - see the LICENSE.md file for details

# Acknowledgments
- [Sherin David](https://gitlab.com/sherindavid)
- [Sruthy S](https://gitlab.com/SruthyS)
- [Abdul Arshad V S](https://gitlab.com/abdularshadvs)
- [Jaison Jacob](https://gitlab.com/jaison_j)

# Changelog

